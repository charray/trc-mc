/*
Copyright (c) 2009-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define _GNU_SOURCE
#include "trcmc.h"
#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <setjmp.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <sched.h>
#include <malloc.h>
#include <stdint.h>
#include <signal.h>

/*
 * Definition of data types.
 */
typedef struct zone {
#ifdef PT_ENABLE
	trc_word_t  commits;
	trc_word_t  aborts;
	trc_word_t  concurrent;
#endif
#ifdef REPORT_ENABLE
	trc_word_t  total_commits;
	trc_word_t  total_aborts;
	trc_word_t  extend_success;
	trc_word_t  extend_failure;
#endif
	trc_word_t  clocks[NUM_ZONES + PAD_ZONES];
} trc_zone_t;

#ifdef REPORT2_ENABLE
static __thread trc_word_t commits;
static __thread trc_word_t aborts;
static __thread trc_word_t extend_success;
static __thread trc_word_t extend_failure;

static trc_word_t global_commits;
static trc_word_t global_aborts;
static trc_word_t global_extend_success;
static trc_word_t global_extend_failure;
#endif

#ifdef PT_ENABLE
typedef union {
	trc_word_t  value;
	struct {
		trc_quarter_t quota;
		trc_quarter_t entered;
		trc_quarter_t peak;
	} actual;
} trc_conn_t;
#endif

typedef struct ops {
	volatile trc_word_t* addr;
	trc_word_t  op;
	trc_word_t  value;
	volatile trc_word_t* lock;
	trc_word_t  timestamp;
	struct ops* prev;
} trc_ops_t;

typedef struct info {
	trc_roll_t* roll;
	trc_word_t  zone;
	trc_word_t  nest;
	trc_word_t  mode;
	trc_word_t  read_only;
	trc_hint_t* hint;
	trc_word_t  clocks[NUM_ZONES];
	trc_ops_t*  ops_base;
	trc_ops_t*  ops_ptr;
	trc_ops_t*  ops_limit;
	trc_word_t  total_commits;
	trc_word_t  total_aborts;
	trc_word_t  consecutive_commits;
	trc_word_t  consecutive_aborts;
} trc_info_t;

typedef union trc_convert {
	trc_word_t word[8 / sizeof(trc_word_t)];
	void*      ptr[8 / sizeof(void*)];
	uint64_t   u64[1];
	int64_t    i64[1];
	uint32_t   u32[2];
	int32_t    i32[2];
	uint16_t   u16[4];
	int16_t    i16[4];
	uint8_t    u8[8];
	int8_t     i8[8];
	double     d[1];
	float      f[2];
} trc_convert_t;

/*
 * Definition of some global and thread variables.
 */
static trc_zone_t zones[NUM_ZONES];
static volatile trc_word_t table[NUM_TIMESTAMPS];
static __thread trc_info_t* my_info;
pthread_mutex_t mutex;

static long* processors;
static long  processor_count;
static long* processor_zones;
static long  zone_members[NUM_ZONES];
static cpu_set_t   affinity[NUM_ZONES];

#ifdef PT_ENABLE
static pthread_t   pt_threads[NUM_ZONES];
#endif

#ifdef DEBUG0_ENABLE
static __thread int my_id;
#endif

trc_word_t counter = 0;

/*
 * Macro definitions for shorter function signatures.
 */

#define _info_      trc_info_t* info
#define _addr_      volatile trc_word_t* addr
#define _op_        trc_word_t  op
#define _old_       trc_word_t  old
#define _value_     trc_word_t  value
#define _lock_      volatile trc_word_t* lock
#define _timestamp_ trc_word_t  timestamp
#define _roll_      trc_roll_t* roll
#define _hint_      trc_hint_t* hint
#define _last_      trc_ops_t*  last

/*
 * Other useful macros.
 */

#define LA 0
#define EA 2
#define AA 3

#define NOP 0
#define READ 8
#define WRITE 9
#define MALLOC 10
#define FREE 11

#define HASH(x)     (((trc_word_t)x >> 3) & (NUM_TIMESTAMPS - 1))
#define LOCK(x)     ((trc_word_t)x | (trc_word_t)1)
#define LOCKER(x)   (trc_ops_t*)((trc_word_t)x & (~(trc_word_t)3))
#define LOCKED(x)   ((trc_word_t)x & (trc_word_t)1)
#define ACQUIRE(x)  ((trc_word_t)x | (trc_word_t)2)
#define ACQUIRED(x) ((trc_word_t)x & (trc_word_t)2)
#define TRIM(x)     ((trc_word_t)x & (~(trc_word_t)3))

#define WORD_BASE(x) \
	(trc_word_t*)((trc_word_t)x & ~(trc_word_t)(sizeof(trc_word_t) - 1))

#define SUBWORD(x,y,z) \
	(((trc_word_t)y - (trc_word_t)x) / (trc_word_t)z)

#ifdef ZONE_ENABLE
#define ZONE(x)     ((trc_word_t)x >> ((sizeof(trc_word_t) * 8) - ZONE_BITS))
#else
#define ZONE(x)     (0)
#endif

#ifdef DEBUG_ENABLE
#define DEBUG(format, ...) printf("thread %d " format "\n",my_id, ##__VA_ARGS__)
#else
#define DEBUG(...)
#endif

#ifdef DEBUG2_ENABLE
#define DEBUG2(format,...) printf("thread %d " format "\n",my_id, ##__VA_ARGS__)
#else
#define DEBUG2(...)
#endif

#ifdef DEBUG3_ENABLE
#define DEBUG3(format,...) printf("thread %d " format "\n",my_id, ##__VA_ARGS__)
#else
#define DEBUG3(...)
#endif

/*
 * Atomic operations.
 */

static trc_word_t cas(_addr_, _old_, _value_) {
	return __sync_val_compare_and_swap(addr, old, value);
}

static trc_word_t inc(_addr_, _value_) {
	return __sync_fetch_and_add(addr, value);
}

/*
static inline void lfence(void) {
	asm volatile ("lfence":::"memory");
}

static inline void sfence(void) {
	asm volatile ("sfence":::"memory");
}

static inline void mfence(void) {
	asm volatile ("mfence":::"memory");
}
*/

static void panic(char* msg) {
	fprintf(stderr, "%s\n", msg);
	abort();
}

/*
 * Internal functions.
 */

static int is_power2(long x) {
        return ((x & (x - 1)) == 0);
}

static long get_count(const long* x) {
	int result;
	result = 0;
	while (*(x++) != -1) ++result;
	return result;
}

static long* get_numbers(const char* str) {
	char* buf;
	char* ptr;
	long* result;
	int  i;
	i = 0;
	buf = strdup(str);
	result = malloc(sizeof(long) * 1);
	for (ptr = strtok(buf, ","); ptr != NULL; ptr = strtok(NULL, ",")) {
		result[i++] = atoi(ptr);
		if (is_power2(i))
			result = realloc(result, sizeof(long) * i * 2);
	}
	result[i] = -1;
	free(buf);
	return result;
}

static void set_zone(long zone) {
	pid_t pid;
	pid = syscall(SYS_gettid);
	if (sched_setaffinity(pid, sizeof(cpu_set_t), &(affinity[zone]))) {
		perror("sched_setaffinity");
	}
}

static trc_word_t assign_zone() {
#ifdef DEBUG0_ENABLE
#ifdef ZONE_ENABLE
	my_id = inc(&counter, 1);
	return processor_zones[my_id % processor_count];
#endif
#endif

#ifdef ZONE_ENABLE
	static trc_word_t counter;
	return processor_zones[inc(&counter, 1) % processor_count];
#endif

#ifdef DEBUG0_ENABLE
	my_id = inc(&counter, 1);
#endif

	return 0;
}

static inline trc_ops_t* query(_info_, _addr_, _op_, _last_) {
	trc_ops_t* ptr;
	if (last) {
		for (ptr = last; ptr; ptr = ptr->prev)
			if (ptr->addr == addr && ptr->op == op)
				return ptr;
	} else {
		for (ptr = info->ops_ptr - 1; ptr >= info->ops_base; --ptr)
			if (ptr->addr == addr && ptr->op == op)
				return ptr;
	}
	return 0;
}

static trc_word_t check(_info_) {
	trc_ops_t* ptr;
	trc_word_t timestamp;
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if (ptr->op != READ) continue;
		timestamp = *(ptr->lock);
		DEBUG("check: %p %lu", ptr->lock, timestamp);
		if (timestamp == ptr->timestamp) continue;
		if (ACQUIRED(timestamp) &&
		    timestamp == TRIM(ptr->timestamp)) continue;
		if (!LOCKED(timestamp)) return 0;
		if (LOCKER(timestamp) < info->ops_base) return 0;
		if (LOCKER(timestamp) >= info->ops_limit) return 0;
	}
	return 1;
}

static trc_word_t check_all(_info_) {
	trc_ops_t* ptr;
	trc_word_t timestamp;
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if (!ptr->op || ptr->op == MALLOC || ptr->op == FREE) continue;
		timestamp = *(ptr->lock);
		DEBUG("check_all: %p %lu", ptr->lock, timestamp);
		if (timestamp == ptr->timestamp) continue;
		if (ACQUIRED(timestamp) &&
		    timestamp == TRIM(ptr->timestamp)) continue;
		if (!LOCKED(timestamp)) return 0;
		if (LOCKER(timestamp) < info->ops_base) return 0;
		if (LOCKER(timestamp) >= info->ops_limit) return 0;
	}
	return 1;
}

static void expand(_info_) {
	trc_ops_t* ptr;
	trc_word_t timestamp;
	trc_ops_t* orig_base;
	trc_word_t orig_ptr;
	trc_word_t orig_size;
	orig_base = info->ops_base;
	orig_ptr = info->ops_ptr - info->ops_base;
	orig_size = info->ops_limit - info->ops_base;
	info->ops_base = realloc(orig_base,
	                         orig_size * sizeof(trc_ops_t) * 2);
	if (!info->ops_base) {
		info->ops_base = orig_base;
		panic("Out of memory for read-write");
	}
	info->ops_ptr = info->ops_base + orig_ptr;
	info->ops_limit = info->ops_base + orig_size * 2;;
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if (!ptr->lock) continue;
		timestamp = *(ptr->lock);
		if (!LOCKED(timestamp)) continue;
		if (LOCKER(timestamp) < orig_base) continue;
		if (LOCKER(timestamp) >= orig_base+orig_size) continue;
		*(ptr->lock) = timestamp - (trc_word_t)orig_base
		               + (trc_word_t)info->ops_base;
	}
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if (!ptr->prev) continue;
		ptr->prev = (trc_ops_t*)(
		            (trc_word_t)ptr->prev -
		            (trc_word_t)orig_base +
			    (trc_word_t)info->ops_base);
	}
}

static inline void insert_read_noprev(_info_, _addr_, _value_, _lock_, _timestamp_) {
	info->ops_ptr->addr = addr;
	info->ops_ptr->op = READ;
	info->ops_ptr->value = value;
	info->ops_ptr->lock = lock;
	info->ops_ptr->timestamp = timestamp;
	info->ops_ptr->prev = 0;
	if (++(info->ops_ptr) == info->ops_limit) expand(info);
}

static inline void insert(_info_, _addr_, _op_, _value_, _lock_, _timestamp_, _last_) {
	trc_ops_t* ptr;
	trc_ops_t* orig_base;
	trc_word_t orig_ptr;
	trc_word_t orig_size;
	info->ops_ptr->addr = addr;
	info->ops_ptr->op = op;
	info->ops_ptr->value = value;
	info->ops_ptr->lock = lock;
	info->ops_ptr->timestamp = timestamp;
	info->ops_ptr->prev = last;
	if (++(info->ops_ptr) == info->ops_limit) {
		orig_base = info->ops_base;
		orig_ptr = info->ops_ptr - info->ops_base;
		orig_size = info->ops_limit - info->ops_base;
		info->ops_base = realloc(orig_base,
		                         orig_size * sizeof(trc_ops_t) * 2);
		if (!info->ops_base) {
			info->ops_base = orig_base;
			panic("Out of memory for read-write");
		}
		info->ops_ptr = info->ops_base + orig_ptr;
		info->ops_limit = info->ops_base + orig_size * 2;;
		for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
			if (!ptr->lock) continue;
			timestamp = *(ptr->lock);
			if (!LOCKED(timestamp)) continue;
			if (LOCKER(timestamp) < orig_base) continue;
			if (LOCKER(timestamp) >= orig_base+orig_size) continue;
			*(ptr->lock) = timestamp - (trc_word_t)orig_base
			               + (trc_word_t)info->ops_base;
		}
		for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
			if (!ptr->prev) continue;
			ptr->prev = (trc_ops_t*)(
			            (trc_word_t)ptr->prev -
			            (trc_word_t)orig_base +
				    (trc_word_t)info->ops_base);
		}
	}
}

static trc_word_t lock(_info_) {
	trc_ops_t* ptr;
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if (ptr->op != WRITE) continue;
		cas(ptr->lock, ptr->timestamp, LOCK(info->ops_base));
		if (*(ptr->lock) != LOCK(info->ops_base)) goto fail;
		DEBUG("lock: %p %lu %lu", ptr->lock,
		      ptr->timestamp, LOCK(info->ops_base));
	}
	return 1;
fail:
	for (--ptr; ptr >= info->ops_base; --ptr) {
		if (ptr->op != WRITE) continue;
		if (*(ptr->lock) != LOCK(info->ops_base)) continue;
		DEBUG("lock cancel: %p %lu %lu", ptr->lock,
		      *(ptr->lock), ptr->timestamp);
		*(ptr->lock) = ptr->timestamp;
	}
	return 0;
}

static void commit(_info_) {
	trc_ops_t* ptr;
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if (ptr->op == WRITE) {
			DEBUG2("commit write: %p %lx %lx", ptr->addr,
			       *(ptr->addr), ptr->value);
			*(ptr->addr) = ptr->value;
		}
		if (ptr->op == FREE) {
			DEBUG2("commit free: %p", ptr->addr);
			free((trc_word_t*)ptr->addr);
		}
	}
}

static void rollback(_info_) {
	trc_ops_t* ptr;
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if (ptr->op == MALLOC) free((trc_word_t*)ptr->addr);
	}
}

static void release(_info_, _timestamp_) {
	trc_ops_t* ptr;
	trc_word_t ts;
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if (ptr->op == WRITE &&
		    LOCKED((ts = *ptr->lock)) &&
		    LOCKER(ts) >= info->ops_base &&
		    LOCKER(ts) < info->ops_ptr)
		{
			DEBUG("release: %p %lu %lu", ptr->lock, ts, timestamp);
			*(ptr->lock) = timestamp;
		}
	}
}

static void release_all(_info_, _timestamp_) {
	trc_ops_t* ptr;
	trc_word_t ts;
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if (ptr->op == WRITE &&
		    LOCKED((ts = *ptr->lock)) &&
		    LOCKER(ts) >= info->ops_base &&
		    LOCKER(ts) < info->ops_ptr)
		{
			*(ptr->lock) = timestamp;
			DEBUG("release_all: %p %lu %lu", ptr->lock,
			      ts, timestamp);
		}
		if (ptr->op == READ &&
		    ACQUIRED((ts = *ptr->lock)))
		{
			*(ptr->lock) = TRIM(ts);
			DEBUG("release_all: %p %lu %lu", ptr->lock,
			      ts, timestamp);
		}
	}
}

static void unlock(_info_) {
	trc_ops_t* ptr;
	trc_word_t ts;
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if (ptr->op == WRITE &&
		    LOCKED((ts = *ptr->lock)) &&
		    LOCKER(ts) >= info->ops_base &&
		    LOCKER(ts) < info->ops_ptr)
		{
			DEBUG("unlock: %p %lu %lu", ptr->lock,
			      *ptr->lock, ptr->timestamp);
			*(ptr->lock) = ptr->timestamp;
		}
	}
}

static void unlock_all(_info_) {
	trc_ops_t* ptr;
	trc_word_t ts;
	for (ptr = info->ops_base; ptr < info->ops_ptr; ++ptr) {
		if ((ptr->op == READ || ptr->op == WRITE) &&
		    (ACQUIRED((ts = *ptr->lock)) ||
		    (LOCKED(ts) &&
		     LOCKER(ts) >= info->ops_base &&
		     LOCKER(ts) < info->ops_ptr)))
		{
			DEBUG("unlock_all: %p %lu %lu", ptr->lock,
			      *ptr->lock, ptr->timestamp);
			*(ptr->lock) = ptr->timestamp;
		}
	}
}

static void clear(_info_) {
	info->ops_ptr = info->ops_base;
}

static trc_info_t* new_info(void) {
	trc_info_t* result;
	result = (trc_info_t*)malloc(sizeof(trc_info_t));
	result->roll = 0;
	result->zone = assign_zone();
	result->nest = 0;
	result->mode = DEFAULT_MODE;
	result->hint = 0;
	memset(result->clocks, 0, sizeof(trc_word_t) * NUM_ZONES);
	result->ops_base = (trc_ops_t*)malloc(sizeof(trc_ops_t) * OPS_COUNT);
	result->ops_ptr = result->ops_base;
	result->ops_limit = result->ops_base + OPS_COUNT;
	result->total_commits = 0;
	result->total_aborts = 0;
	result->consecutive_commits = 0;
	result->consecutive_aborts = 0;
	return result;
}

static void delete_info(_info_) {
	if (info->nest != 0) panic("Deleting unfinished transactions.");
	free(info->ops_base);
	free(info);
}

static void register_info(_info_) {
	my_info = info;
	if (info) set_zone(info->zone);
}

static trc_info_t* current_info(void) {
	if (!my_info) register_info(new_info());
	return my_info;
}

#ifdef PT_ENABLE
static void* pt_thread(void* args) {
	trc_word_t my_success;
	trc_word_t my_success2;
	trc_word_t my_collides;
	trc_conn_t old;
	trc_conn_t new;
	trc_word_t ratio;
	trc_word_t direction = 0;
	trc_word_t last_count = 0;
	trc_word_t last_cycle = 0;
	trc_word_t this_count = 0;
	trc_word_t this_cycle = 0;
	trc_word_t my_zone;

	my_zone = (long)args;
	set_zone(my_zone);

#ifdef DEBUG4_ENABLE
	printf("PT Thread for zone %ld started.\n", my_zone);
#endif

	while (1) {
		usleep(PT_PERIOD);
UPDATE_RESTART:
		my_success = zones[my_zone].commits;
		my_collides = zones[my_zone].aborts;
		// mfence();
		my_success2 = zones[my_zone].commits;
		if (my_success != my_success2)
			goto UPDATE_RESTART;

		old.value = zones[my_zone].concurrent;
		new.value = old.value;
		if (old.actual.entered || old.actual.peak) {
			this_cycle += 1;
		} else {
			continue;
		}
		if (my_success + my_collides < 10) {
			continue;
		} else {
			this_count = my_success;
		}
		ratio = my_success * PT_DENOM / (my_success + my_collides);
		if (new.actual.quota < 2) {
			direction = 1;
		} else if (new.actual.peak &&
		           new.actual.peak < new.actual.quota)
		{
			new.actual.quota = new.actual.peak + 1;
			direction = 0;
		} else if (ratio >= PT_UPPER) {
			direction = 1;
		} else if (ratio < PT_LOWER) {
			direction = 0;
		} else if (this_count * last_cycle < last_count * this_cycle) {
			direction = direction? 0: 1;
		}
		if (direction == 0)
			new.actual.quota -= 1;
		else
			new.actual.quota += 1;

		if (new.actual.quota < 2)
			new.actual.quota = 1;

		new.actual.peak = 0;
		if (old.value != cas(&(zones[my_zone].concurrent),
		                     old.value, new.value))
			goto UPDATE_RESTART;

		zones[my_zone].commits = 0;
		zones[my_zone].aborts = 0;

#ifdef DEBUG4_ENABLE
		if (old.actual.quota != new.actual.quota) {
			printf("Zone %ld: %d->%d (ratio: %ld, rate:%ld->%ld,"
			       "peak: %d)\n",
			       my_zone,
			       old.actual.quota,
			       new.actual.quota,
			       ratio,
			       last_count * this_cycle,
			       this_count * last_cycle,
			       old.actual.peak);
		}
#endif

		last_count = this_count;
		last_cycle = this_cycle;
		this_count = 0;
		this_cycle = 0;
	}
	return 0;
}

static void pt_init() {
	trc_word_t i;
	trc_conn_t conn;
	for (i = 0; i < NUM_ZONES; ++i) {
		conn.value = 0;
		conn.actual.quota = zone_members[i];
		zones[i].concurrent = conn.value;
		if (zone_members[i] != 0) {
			pthread_create(pt_threads + i, 0, pt_thread, (void*)i);
		}
	}
}

static void pt_begin(_info_) {
	trc_word_t old;
	trc_word_t newv;
	trc_conn_t conn;
	old = conn.value = zones[info->zone].concurrent;
	while (1) {
		if (conn.actual.entered >= conn.actual.quota) {
			sched_yield();
			old = conn.value = zones[info->zone].concurrent;
			continue;
		}
		conn.actual.entered += 1;
		if (conn.actual.entered > conn.actual.peak)
			conn.actual.peak = conn.actual.entered;
		newv = cas(&(zones[info->zone].concurrent), old, conn.value);
		if (newv == old) break;
		else old = conn.value = newv;
	}
}

static void pt_commit(_info_) {
	trc_word_t old;
	trc_word_t newv;
	trc_conn_t conn;
	inc(&(zones[info->zone].commits), 1);
	old = conn.value = (zones[info->zone].concurrent);
	while (1) {
		conn.actual.entered -= 1;
		newv = cas(&(zones[info->zone].concurrent), old, conn.value);
		if (newv == old) break;
		else old = conn.value = newv;
	}
}

static void pt_abort(_info_) {
	trc_word_t old;
	trc_word_t newv;
	trc_conn_t conn;
	inc(&(zones[info->zone].aborts), 1);
	old = conn.value = (zones[info->zone].concurrent);
	while (1) {
		conn.actual.entered -= 1;
		newv = cas(&(zones[info->zone].concurrent), old, conn.value);
		if (newv == old) break;
		else old = conn.value = newv;
	}
}

static void pt_destroy() {
	trc_word_t i;
	for (i = 0; i < NUM_ZONES; ++i)
		if (zone_members[i] != 0)
			pthread_cancel(pt_threads[i]);
}
#endif

#ifdef MS_ENABLE
static void ms_init() {

}

static void ms_begin(_info_) {

}

static void ms_commit(_info_) {

}

static void ms_abort(_info_) {

}

static void ms_destroy() {

}
#endif

static void trc_sigsegv(int signal_no) {
	trc_info_t* info = current_info();
	if (!info->nest)
		panic("Unexpected sigsegv/sigbus signal.");
	DEBUG("Signal %d caught and skipped.", signal_no);
	trc_abort();
	signal(signal_no, trc_sigsegv);
}

void trc_init(int* argc, char*** argv) {
	char* processor_str;
	char* zone_str;
	trc_word_t i, j;
	for (i = 0; i < NUM_ZONES; ++i) {
		for (j = 0; j < NUM_ZONES; ++j) {
			zones[i].clocks[j] =
			(j << (sizeof(trc_word_t) * 8 - ZONE_BITS));
		}
	}

	if (sizeof(trc_word_t) != 4 * sizeof(trc_quarter_t)) {
		panic("Configuration error. Word and Quarter type mismatch.");
	}

	if (sizeof(trc_word_t) != 4 && sizeof(trc_word_t) != 8) {
		panic("Unsupported word size.");
	}

	if (pthread_mutex_init(&mutex, 0))
		perror("pthread_mutex_init");

	processor_str = getenv("PROCESSOR_IDS")?
		getenv("PROCESSOR_IDS"): PROCESSORS;
	zone_str = getenv("PROCESSOR_ZONES")?
		getenv("PROCESSOR_ZONES"): ZONES;
	processors = get_numbers(processor_str);
	processor_count = get_count(processors);
	processor_zones = get_numbers(zone_str);
	if (processor_count == 0) {
		fprintf(stderr, "PROCESSOR_IDS not set\n");
		exit(1);
	} else if (get_count(processor_zones) != processor_count) {
		fprintf(stderr, "PROCESSOR_IDS mismatch PROCESSOR_ZONES\n");
		exit(1);
	}
	for (i = 0; i < processor_count; ++i) {
		if (processor_zones[i] >= NUM_ZONES) {
			fprintf(stderr, "ZONE ID exceed" 
			        "the allowed number of %d",
				NUM_ZONES - 1);
			exit(1);
		}
	}

	for (i = 0; i < NUM_ZONES; ++i) {
		CPU_ZERO(&(affinity[i]));
		zone_members[i] = 0;
		for (j = 0; j < processor_count; ++j) {
			if (processor_zones[j] == i) {
				CPU_SET(processors[j], &(affinity[i]));
				zone_members[i] += 1;
			}
		}
	}

#ifdef PT_ENABLE
	pt_init();
#endif
#ifdef MS_ENABLE
	ms_init();
#endif
	signal(SIGBUS, trc_sigsegv);
	signal(SIGSEGV, trc_sigsegv);
}

void trc_destroy() {
#ifdef PT_ENABLE
	pt_destroy();
#endif
#ifdef MS_ENABLE
	ms_destroy();
#endif
#ifdef REPORT_ENABLE
	trc_word_t i;
	trc_word_t total_commits = 0;
	trc_word_t total_aborts = 0;
	trc_word_t extend_success = 0;
	trc_word_t extend_failure = 0;
	for (i = 0; i < NUM_ZONES; ++i) {
		if (zones[i].total_commits == 0 && zones[i].total_aborts == 0)
			continue;
		printf("Zone %ld commits: %ld, extend success: %ld, "
		       "extend failure: %ld, aborts: %ld\n",
		       i, zones[i].total_commits, zones[i].extend_success,
		       zones[i].extend_failure, zones[i].total_aborts);
		total_commits += zones[i].total_commits;
		total_aborts += zones[i].total_aborts;
		extend_success += zones[i].extend_success;
		extend_failure += zones[i].extend_failure;
	}
	printf("Total commits: %ld, extend success: %ld, "
	       "extend failure: %ld, aborts: %ld\n",
	       total_commits, extend_success, extend_failure, total_aborts);
#endif
#ifdef REPORT2_ENABLE
	printf("Total commits: %ld, extend success: %ld, "
	       "extend failure: %ld, aborts: %ld\n",
	       global_commits, global_extend_success,
	       global_extend_failure, global_aborts);
#endif
}

void trc_thread_init() {
	current_info();
	signal(SIGBUS, trc_sigsegv);
	signal(SIGSEGV, trc_sigsegv);
}

void trc_thread_destroy() {
	if (current_info()) {
		delete_info(current_info());
		register_info(0);
	}
#ifdef REPORT2_ENABLE
	inc(&global_commits, commits);
	inc(&global_aborts, aborts);
	inc(&global_extend_success, extend_success);
	inc(&global_extend_failure, extend_failure);
#endif
}

static void get_timestamp(_info_) {
	memcpy(info->clocks, zones[info->zone].clocks, sizeof(info->clocks));
}

static trc_word_t new_timestamp(trc_word_t x) {
	trc_word_t result;
	result = inc(&(zones[x].clocks[x]), 4);
	if (ZONE(result) != x) panic("Timestamp overflow");
	return result + 4;
}

static void share_timestamp(trc_word_t home, trc_word_t zone, trc_word_t ts) {
	trc_word_t orig;
	trc_word_t newv;
	orig = zones[home].clocks[zone];
	if (ZONE(ts) != zone) panic("Sharing incorrect timestamp");
	if (home == zone) return;
retry:
	if (orig > ts) return;
	newv = cas(&(zones[home].clocks[zone]), orig, ts);
	if (newv != orig) {
		orig = newv;
		goto retry;
	}
}

static void notify_entrance(_info_) {
#ifdef PT_ENABLE
	pt_begin(info);
#endif

#ifdef MS_ENABLE
	ms_begin(info);
#endif
}

static void notify_commit(_info_) {
#ifdef PT_ENABLE
	pt_commit(info);
#endif

#ifdef MS_ENABLE
	ms_commit(info);
#endif

#ifdef HINT_ENABLE
	if (info->hint) inc(&(info->hint->commits), 1);
	info->consecutive_commits += 1;
	info->consecutive_aborts = 0;
	info->total_commits += 1;
#endif

#ifdef REPORT_ENABLE
	inc(&(zones[info->zone].total_commits), 1);
#endif
#ifdef REPORT2_ENABLE
	commits += 1;
#endif
}

static void notify_abort(_info_) {
#ifdef PT_ENABLE
	pt_abort(info);
#endif

#ifdef MS_ENABLE
	ms_abort(info);
#endif

#ifdef HINT_ENABLE
	if (info->hint) inc(&(info->hint->aborts), 1);
	info->consecutive_commits = 0;
	info->consecutive_aborts += 1;
	info->total_aborts += 1;
#endif

#ifdef REPORT_ENABLE
	inc(&(zones[info->zone].total_aborts), 1);
#endif
#ifdef REPORT2_ENABLE
	aborts += 1;
#endif
}

static void notify_extend_success(_info_) {
#ifdef REPORT_ENABLE
	inc(&(zones[info->zone].extend_success), 1);
#endif
#ifdef REPORT2_ENABLE
	extend_success += 1;
#endif
}

static void notify_extend_failure(_info_) {
#ifdef REPORT_ENABLE
	inc(&(zones[info->zone].extend_failure), 1);
#endif
#ifdef REPORT2_ENABLE
	extend_failure += 1;
#endif
}

/*
 * Mode-dependent functions.
 */
void trc_begin_aa(_roll_, _hint_) {
	trc_info_t* info;
	info = current_info();
	if (++(info->nest) > 1) return;
	while (pthread_mutex_lock(&mutex)) perror("pthread_mutex_lock");
	info->roll = roll;
	info->hint = hint;
	info->read_only = 1;
	notify_entrance(info);
	get_timestamp(info);
	// mfence();
	DEBUG("begin_aa");
}

void trc_begin_ea(_roll_, _hint_) {
	trc_info_t* info;
	info = current_info();
	if (++(info->nest) > 1) return;
	info->roll = roll;
	info->hint = hint;
	info->read_only = 1;
	notify_entrance(info);
	get_timestamp(info);
	// mfence();
	DEBUG("begin_ea");
}

void trc_begin_la(_roll_, _hint_) {
	trc_info_t* info;
	info = current_info();
	if (++(info->nest) > 1) return;
	info->roll = roll;
	info->hint = hint;
	info->read_only = 1;
	notify_entrance(info);
	get_timestamp(info);
	// mfence();
	DEBUG("begin_la");
}

void trc_abort_aa(void) {
	trc_info_t* info;
	info = current_info();
	DEBUG("abort_aa begin");
	if (!info->read_only) {
		// mfence();
		rollback(info);
		// mfence();
	}
	unlock_all(info);
	// sfence();
	notify_abort(info);
	clear(info);
	info->nest = 0;
	while (pthread_mutex_unlock(&mutex)) perror("pthread_mutex_unlock");
	DEBUG("abort_aa end");
	siglongjmp(*(info->roll), 0);
}

void trc_abort_ea(void) {
	trc_info_t* info;
	info = current_info();
	DEBUG("abort_ea begin");
	if (!info->read_only) {
		// mfence();
		rollback(info);
		// mfence();
		unlock(info);
		// sfence();
	}
	notify_abort(info);
	clear(info);
	info->nest = 0;
	DEBUG("abort_ea end");
	siglongjmp(*(info->roll), 0);
}

void trc_abort_la(void) {
	trc_info_t* info;
	info = current_info();
	DEBUG("abort_la begin");
	if (!info->read_only) {
		// mfence();
		rollback(info);
		// sfence();
	}
	notify_abort(info);
	clear(info);
	info->nest = 0;
	DEBUG("abort_la end");
	siglongjmp(*(info->roll), 0);
}

void trc_commit_aa(void) {
	trc_info_t* info;
	trc_word_t timestamp;
	info = current_info();
	DEBUG("commit_aa begin");
	if (info->nest <= 0) panic("Empty commits");
	if (--(info->nest) >= 1) return;
	timestamp = new_timestamp(info->zone);
	DEBUG("commit_aa: %lu", timestamp);
	if (!info->read_only) {
		// mfence();
		commit(info);
		// sfence();
	}
	release_all(info, timestamp);
	while (pthread_mutex_unlock(&mutex)) perror("pthread_mutex_unlock");
	notify_commit(info);
	DEBUG("commit_aa end");
	clear(info);
}

void trc_commit_ea(void) {
	trc_info_t* info;
	trc_word_t timestamp;
	info = current_info();
	DEBUG("commit_ea begin");
	if (info->nest <= 0) panic("Empty commits");
	if (--(info->nest) >= 1) return;
	if (!info->read_only) {
		// mfence();
		if (!check(info)) trc_abort_ea();
		timestamp = new_timestamp(info->zone);
		DEBUG("commit_ea: %lu", timestamp);
		// mfence();
		commit(info);
		// sfence();
		release(info, timestamp);
		// sfence();
	} else {
		DEBUG("commit_ea read only skip");
	}
	notify_commit(info);
	DEBUG("commit_ea end");
	clear(info);
}

void trc_commit_la(void) {
	trc_info_t* info;
	trc_word_t timestamp;
	info = current_info();
	DEBUG("commit_la begin");
	if (info->nest <= 0) panic("Empty commits");
	if (--(info->nest) >= 1) return;
	if (!info->read_only) {
		// mfence();
		if (!lock(info)) trc_abort_la();
		// mfence();
		if (!check(info)) trc_abort_ea();
		timestamp = new_timestamp(info->zone);
		DEBUG("commit_la: %lu", timestamp);
		// mfence();
		commit(info);
		// sfence();
		release(info, timestamp);
		// sfence();
	} else {
		DEBUG("commit_la read_only_skip");
	}
	notify_commit(info);
	DEBUG("commit_la end");
	clear(info);
}

trc_word_t trc_read_aa(_addr_) {
	trc_info_t* info;
	volatile trc_word_t* lock;
	trc_word_t timestamp;
	trc_word_t new_timestamp;
	trc_word_t value;
	trc_ops_t* locker;
	info = current_info();
	lock = table + HASH(addr);
	timestamp = *lock;
	// mfence();
retry:
	if (ACQUIRED(timestamp)) {
		value = *addr;
		insert_read_noprev(info, addr, value, lock, timestamp);
		DEBUG("read_aa acquired: %p %lu", lock, timestamp);
	} else if (LOCKED(timestamp)) {
		locker = LOCKER(timestamp);
		if (locker < info->ops_base || locker >= info->ops_limit) {
			timestamp = *lock;
			goto retry;
		}
		DEBUG("read_aa self: %p %lu", lock, timestamp);
		if ((locker = query(info, addr, WRITE, locker)))
			value = locker->value;
		else {
			value = *addr;
			insert_read_noprev(info, addr, value, lock, timestamp);
		}
	} else {
		new_timestamp = cas(lock, timestamp, ACQUIRE(timestamp));
		if (new_timestamp != timestamp) {
			timestamp = new_timestamp;
			goto retry;
		}
		DEBUG("read_aa action: %p %lu %lu", lock,
		      timestamp, ACQUIRE(timestamp));
		value = *addr;
		insert_read_noprev(info, addr, value, lock, timestamp);
	}
	DEBUG("READ %p value 0x%lx", addr, (long)value);
	return value;
}

trc_word_t trc_read_ea(_addr_) {
	trc_info_t* info;
	volatile trc_word_t* lock;
	trc_word_t timestamp;
	trc_ops_t* locker;
	trc_word_t value;
	trc_word_t zone;
	info = current_info();
	lock = table + HASH(addr);
retry:
	timestamp = *lock;
	value = *addr;
	if (timestamp != *lock) {
		goto retry;
	}
	if (LOCKED(timestamp)) {
		locker = LOCKER(timestamp);
		if (locker<info->ops_base || locker>=info->ops_limit) {
			DEBUG("read_ea contended: %p %lu", lock, timestamp);
			trc_abort_ea();
		}
		if ((locker = query(info, addr, WRITE, locker)) != 0) {
			DEBUG("read_ea self: %p %lu", lock, timestamp);
			DEBUG("READ %p value 0x%lx", addr, (long)locker->value);
			return locker->value;
		}
		DEBUG("read_ea self: %p %lu", lock, timestamp);
		insert_read_noprev(info, addr, value, lock, timestamp);
		DEBUG("READ %p value 0x%lx", addr, (long)value);
		return value;
	}
	zone = ZONE(timestamp);
	timestamp = TRIM(timestamp);
	if (info->clocks[zone] < timestamp) {
		share_timestamp(info->zone, zone, timestamp);
		DEBUG("read_ea extend");
#ifdef TEXT_ENABLE
		if (!check(info)) {
			notify_extend_failure(info);
			trc_abort_ea();
		}
		notify_extend_success(info);
		info->clocks[zone] = timestamp;
		goto retry;
#else
		DEBUG("read_ea no extend");
		trc_abort_ea();
		goto retry;
#endif
	}
	DEBUG("read_ea normal %p %lu", lock, timestamp);
	DEBUG("READ %p value 0x%lx", addr, (long)value);
	insert_read_noprev(info, addr, value, lock, timestamp);
	return value;
}

trc_word_t trc_read_la(_addr_) {
	trc_info_t* info;
	volatile trc_word_t* lock;
	trc_word_t timestamp;
	trc_ops_t* locker;
	trc_word_t value;
	trc_word_t zone;
	info = current_info();
	lock = table + HASH(addr);
retry:
	timestamp = *lock;
	value = *addr;
	if (timestamp != *lock) {
		goto retry;
	}
	if (LOCKED(timestamp)) {
		DEBUG("read_la contended: %p %lu", lock, timestamp);
		trc_abort_la();
	}
	zone = ZONE(timestamp);
	timestamp = TRIM(timestamp);
	if (info->clocks[zone] < timestamp) {
		share_timestamp(info->zone, zone, timestamp);
#ifdef TEXT_ENABLE
		DEBUG("read_la extend");
		if (!check_all(info)) {
			notify_extend_failure(info);
			trc_abort_la();
		}
		notify_extend_success(info);
		info->clocks[zone] = timestamp;
		goto retry;
#else
		DEBUG("read_la no extend");
		trc_abort_la();
		goto retry;
#endif
	} else if ((locker = query(info, addr, WRITE, 0))) {
		DEBUG("read_la self: %p %lu", lock, timestamp);
		DEBUG("READ: %p value 0x%lx", addr, (long)locker->value);
		return locker->value;
	} else {
		insert_read_noprev(info, addr, value, lock, timestamp);
		DEBUG("read_la normal: %p %ld", lock, timestamp);
		DEBUG("READ: %p value 0x%lx", addr, (long)value);
		return value;
	}
}

void trc_write_aa(_addr_, _value_) {
	trc_info_t* info;
	trc_ops_t* locker;
	volatile trc_word_t* lock;
	trc_word_t timestamp;
	trc_word_t new_timestamp;
	info = current_info();
	lock = table + HASH(addr);
	timestamp = *lock;
retry:
	if (LOCKED(timestamp)) {
		locker = LOCKER(timestamp);
		if (locker < info->ops_base || locker >= info->ops_limit) {
			timestamp = *lock;
			goto retry;
		}
		DEBUG("write_aa self: %p %lu %lu", lock, *lock,
		      LOCK(info->ops_ptr));
		DEBUG("WRITE %p value 0x%lx", addr, (long)value);
		*lock = LOCK(info->ops_ptr);
		insert(info, addr, WRITE, value, lock, timestamp, locker);
	} else {
		new_timestamp = cas(lock, timestamp, LOCK(info->ops_ptr));
		if (new_timestamp != timestamp) {
			timestamp = new_timestamp;
			goto retry;
		}
		DEBUG("write_aa lock: %p %ld %ld", lock, timestamp,
		      LOCK(info->ops_ptr));
		DEBUG("WRITE %p value 0x%lx", addr, (long)value);
		insert(info, addr, WRITE, value, lock, TRIM(timestamp), 0);
	}
	info->read_only = 0;
}

void trc_write_ea(_addr_, _value_) {
	trc_info_t* info;
	volatile trc_word_t* lock;
	trc_word_t timestamp;
	trc_word_t new_timestamp;
	trc_word_t zone;
	trc_ops_t* locker;
	info = current_info();
	lock = table + HASH(addr);
	timestamp = *lock;
	zone = ZONE(timestamp);
retry:
	if (ACQUIRED(timestamp)) {
		DEBUG("write_ea acquired: %p %ld", lock, timestamp);
		trc_abort_ea();
	} else if (LOCKED(timestamp)) {
		locker = LOCKER(timestamp);
		if (locker < info->ops_base || locker >= info->ops_limit) {
			DEBUG("write_ea contended: %p %ld", lock, timestamp);
			trc_abort_ea();
		}
		DEBUG("write_ea self: %p %ld %ld", lock, *lock,
		      LOCK(info->ops_ptr));
		DEBUG("WRITE %p value 0x%lx", addr, (long)value);
		*lock = LOCK(info->ops_ptr);
		insert(info, addr, WRITE, value, lock, timestamp, locker);
	} else if (info->clocks[zone] < timestamp) {
		share_timestamp(info->zone, zone, timestamp);
#ifdef TEXT_ENABLE
		DEBUG("write_ea extend");
		if (!check(info)) {
			notify_extend_failure(info);
			trc_abort_ea();
		}
		notify_extend_success(info);
		info->clocks[zone] = timestamp;
		timestamp = *lock;
		zone = ZONE(timestamp);
		goto retry;
#else
		DEBUG("write_ea no extend");
		trc_abort_ea();
		goto retry;
#endif
	} else {
		new_timestamp = cas(lock, timestamp, LOCK(info->ops_ptr));
		if (new_timestamp != timestamp) {
			timestamp = new_timestamp;
			zone = ZONE(timestamp);
			goto retry;
		}
		DEBUG("write_ea lock: %p %lu %lu", lock, timestamp,
		      LOCK(info->ops_ptr));
		DEBUG("WRITE %p value 0x%lx", addr, (long)value);
		insert(info, addr, WRITE, value, lock, timestamp, 0);
	}
	info->read_only = 0;
}

void trc_write_la(_addr_, _value_) {
	trc_info_t* info;
	info = current_info();
	volatile trc_word_t* lock;
	trc_word_t timestamp;
	trc_word_t zone;
	lock = table + HASH(addr);
retry:
	timestamp = *lock;
	zone = ZONE(timestamp);
	if (ACQUIRED(timestamp) || LOCKED(timestamp)) {
		DEBUG("write_la contended: %p %ld", lock, timestamp);
		trc_abort_la();
	} else if (info->clocks[zone] < timestamp) {
		share_timestamp(info->zone, zone, timestamp);
#ifdef TEXT_ENABLE
		DEBUG("write_la extend");
		if (!check_all(info)) {
			notify_extend_failure(info);
			trc_abort_la();
		}
		notify_extend_success(info);
		info->clocks[zone] = timestamp;
		goto retry;
#else
		DEBUG("write_la no extend");
		trc_abort_la();
		goto retry;
#endif
	}
	insert(info, addr, WRITE, value, lock, timestamp, 0);
	DEBUG("WRITE: %p value 0x%ld", addr, (long)value);
	info->read_only = 0;
}

/*
 * Public functions.
 */

void trc_begin(_roll_, _hint_) {
	switch(current_info()->mode) {
	case LA:
		trc_begin_la(roll, hint);
		break;
	case EA:
		trc_begin_ea(roll, hint);
		break;
	case AA:
		trc_begin_aa(roll, hint);
		break;
	default:
		panic("Unknown mode in trc_begin");
	}
}

void trc_commit(void) {
	switch(current_info()->mode) {
	case LA:
		trc_commit_la();
		break;
	case EA:
		trc_commit_ea();
		break;
	case AA:
		trc_commit_aa();
		break;
	default:
		panic("Unknown mode in trc_commit");
	}
}

void trc_abort(void) {
	switch(current_info()->mode) {
	case LA:
		trc_abort_la();
		break;
	case EA:
		trc_abort_ea();
		break;
	case AA:
		trc_abort_aa();
		break;
	default:
		panic("Unknown mode in trc_abort");
	}
}


trc_word_t trc_read(_addr_) {
#ifdef MS_ENABLE
	switch(current_info()->mode) {
	case LA:
		return trc_read_la(addr);
	case EA:
		return trc_read_ea(addr);
	case AA:
		return trc_read_aa(addr);
	}
#elif (DEFAULT_MODE==LA)
	return trc_read_la(addr);
#elif (DEFAULT_MODE==EA)
	return trc_read_ea(addr);
#elif (DEFAULT_MODE==AA)
	return trc_read_aa(addr);
#endif
	panic("Unknown mode in trc_read");
	return 0;
}

void trc_write(_addr_, _value_) {
#ifdef MS_ENABLE
	switch(current_info()->mode) {
	case LA:
		trc_write_la(addr, value);
		break;
	case EA:
		trc_write_ea(addr, value);
		break;
	case AA:
		trc_write_aa(addr, value);
		break;
	default:
		panic("Unknown mode in trc_write");
	}
#elif (DEFAULT_MODE==LA)
	trc_write_la(addr, value);
#elif (DEFAULT_MODE==EA)
	trc_write_ea(addr, value);
#elif (DEFAULT_MODE==AA)
	trc_write_aa(addr, value);
#endif
}

trc_word_t* trc_malloc(trc_word_t size) {
	trc_info_t* info;
	trc_word_t* result;
	info = current_info();
	info->read_only = 0;
	size = ((size + 7) & (~(trc_word_t)7));
	if (info->nest == 0) {
		result = malloc(size);
		DEBUG("MALLOC DIRECT: %p", result);
		return result;
	}
	result = malloc(size);
	if (!result) panic("Insufficient memory");
	insert(info, result, MALLOC, 0, 0, 0, 0);
	DEBUG("MALLOC: %p", result);
	return result;
}

void trc_free2(trc_word_t* addr, trc_word_t size) {
	trc_info_t* info;
	trc_word_t* ptr;
	info = current_info();
	info->read_only = 0;
	size = size / sizeof(trc_word_t);
	DEBUG("FREE: %p", addr);
	for (ptr = addr; ptr < addr + size; ++ptr) trc_write(ptr, 0);
	insert(info, addr, FREE, 0, 0, 0, 0);
	DEBUG("FREE completed");
}

void trc_free(trc_word_t* addr) {
	trc_info_t* info;
	info = current_info();
	if (info->nest == 0) {
		DEBUG("FREE DIRECT: %p", addr);
		return free(addr);
	}
	trc_free2(addr, malloc_usable_size(addr));
}

trc_word_t* trc_realloc(trc_word_t* addr, trc_word_t new_size) {
	trc_info_t* info;
	trc_word_t* result;
	trc_word_t  size;
	info = current_info();
	DEBUG("REALLOC: %p", addr);
	if (info->nest == 0) return realloc(addr, new_size);
	size = malloc_usable_size(addr);
	result = trc_malloc(new_size);
	memcpy(result, addr, size);
	trc_free(addr);
	DEBUG("REALLOC: %p %p", addr, result);
	return result;
}

void* trc_read_ptr(volatile void** addr) {
	trc_convert_t convert;
	trc_word_t* translated;
	translated = (trc_word_t*)addr;
	convert.word[0] = trc_read(translated);
	return convert.ptr[0];
}

uint64_t trc_read_u64(volatile uint64_t* addr) {
	if (sizeof(trc_word_t) == 8) {
		return trc_read((trc_word_t*)addr);
	} else {
		trc_convert_t convert;
		trc_word_t* translated;
		translated = (trc_word_t*)addr;
		convert.word[0] = trc_read(translated);
		convert.word[1] = trc_read(translated + 1);
		return convert.u64[0];
	}
}

uint32_t trc_read_u32(volatile uint32_t* addr) {
	if (sizeof(trc_word_t) == 4) {
		return trc_read((trc_word_t*)addr);
	} else {
		trc_word_t* translated;
		trc_word_t  offset;
		trc_convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		return convert.u32[offset];
	}
}

uint16_t trc_read_u16(volatile uint16_t* addr) {
	trc_word_t* translated;
	trc_word_t offset;
	trc_convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 2);
	convert.word[0] = trc_read(translated);
	return convert.u16[offset];
}

uint8_t trc_read_u8(volatile uint8_t* addr) {
	trc_word_t* translated;
	trc_word_t offset;
	trc_convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 1);
	convert.word[0] = trc_read(translated);
	return convert.u8[offset];
}

int64_t trc_read_i64(volatile int64_t* addr) {
	if (sizeof(trc_word_t) == 8) {
		return (int64_t)trc_read((trc_word_t*)addr);
	} else {
		trc_convert_t convert;
		trc_word_t* translated;
		translated = (trc_word_t*)addr;
		convert.word[0] = trc_read(translated);
		convert.word[1] = trc_read(translated + 1);
		return convert.i64[0];
	}
}

int32_t trc_read_i32(volatile int32_t* addr) {
	if (sizeof(trc_word_t) == 4) {
		return (int32_t)trc_read((trc_word_t*)addr);
	} else {
		trc_word_t* translated;
		trc_word_t  offset;
		trc_convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		return convert.i32[offset];
	}
}

int16_t trc_read_i16(volatile int16_t* addr) {
	trc_word_t* translated;
	trc_word_t offset;
	trc_convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 2);
	convert.word[0] = trc_read(translated);
	return convert.i16[offset];
}

int8_t trc_read_i8(volatile int8_t* addr) {
	trc_word_t* translated;
	trc_word_t offset;
	trc_convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 1);
	convert.word[0] = trc_read(translated);
	return convert.i8[offset];
}

double trc_read_double(volatile double* addr) {
	if (sizeof(trc_word_t) == 8) {
		trc_convert_t convert;
		convert.word[0] = trc_read((trc_word_t*)addr);
		return convert.d[0];
	} else {
		trc_convert_t convert;
		trc_word_t* translated;
		translated = (trc_word_t*)addr;
		convert.word[0] = trc_read(translated);
		convert.word[1] = trc_read(translated + 1);
		return convert.d[0];
	}
}

float trc_read_float(volatile float* addr) {
	if (sizeof(trc_word_t) == 4) {
		trc_convert_t convert;
		convert.word[0] = trc_read((trc_word_t*)addr);
		return convert.f[0];
	} else {
		trc_word_t* translated;
		trc_word_t  offset;
		trc_convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		return convert.f[offset];
	}
}

void trc_write_ptr(volatile void** addr, void* value) {
	trc_convert_t convert;
	trc_word_t* translated;
	translated = (trc_word_t*)addr;
	convert.ptr[0] = value;
	trc_write(translated, convert.word[0]);
}

void trc_write_u64(volatile uint64_t* addr, uint64_t value) {
	if (sizeof(trc_word_t) == 8) {
		return trc_write((trc_word_t*)addr, value);
	} else {
		trc_convert_t convert;
		trc_word_t* translated;
		translated = (trc_word_t*)addr;
		convert.u64[0] = value;
		trc_write(translated, convert.word[0]);
		trc_write(translated + 1, convert.word[1]);
	}
}

void trc_write_u32(volatile uint32_t* addr, uint32_t value) {
	if (sizeof(trc_word_t) == 4) {
		return trc_write((trc_word_t*)addr, value);
	} else {
		trc_word_t* translated;
		trc_word_t offset;
		trc_convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		DEBUG3("write_u32 read: %p %lx", translated, convert.word[0]);
		convert.u32[offset] = value;
		DEBUG3("write_u32 write: %p %lx", translated, convert.word[0]);
		trc_write(translated, convert.word[0]);
	}
}

void trc_write_u16(volatile uint16_t* addr, uint16_t value) {
	trc_word_t* translated;
	trc_word_t offset;
	trc_convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 2);
	convert.word[0] = trc_read(translated);
	DEBUG3("write_u16 read: %p %lx", translated, convert.word[0]);
	convert.u16[offset] = value;
	DEBUG3("write_u16 write: %p %lx", translated, convert.word[0]);
	trc_write(translated, convert.word[0]);
}

void trc_write_u8(volatile uint8_t* addr, uint8_t value) {
	trc_word_t* translated;
	trc_word_t offset;
	trc_convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 1);
	convert.word[0] = trc_read(translated);
	DEBUG3("write_u8 read: %p %lx", translated, convert.word[0]);
	convert.u8[offset] = value;
	DEBUG3("write_u8 write: %p %lx", translated, convert.word[0]);
	trc_write(translated, convert.word[0]);
}

void trc_write_i64(volatile int64_t* addr, int64_t value) {
	if (sizeof(trc_word_t) == 8) {
		return trc_write((trc_word_t*)addr, (trc_word_t)value);
	} else {
		trc_convert_t convert;
		trc_word_t* translated;
		translated = (trc_word_t*)addr;
		convert.i64[0] = value;
		trc_write(translated, convert.word[0]);
		trc_write(translated + 1, convert.word[1]);
	}
}

void trc_write_i32(volatile int32_t* addr, int32_t value) {
	if (sizeof(trc_word_t) == 4) {
		return trc_write((trc_word_t*)addr, value);
	} else {
		trc_word_t* translated;
		trc_word_t offset;
		trc_convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		DEBUG3("write_i32 read: %p %lx", translated, convert.word[0]);
		convert.i32[offset] = value;
		DEBUG3("write_i32 write: %p %lx", translated, convert.word[0]);
		trc_write(translated, convert.word[0]);
	}
}

void trc_write_i16(volatile int16_t* addr, int16_t value) {
	trc_word_t* translated;
	trc_word_t offset;
	trc_convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 2);
	convert.word[0] = trc_read(translated);
	DEBUG3("write_i16 read: %p %lx", translated, convert.word[0]);
	convert.i16[offset] = value;
	DEBUG3("write_i16 write: %p %lx", translated, convert.word[0]);
	trc_write(translated, convert.word[0]);
}

void trc_write_i8(volatile int8_t* addr, int8_t value) {
	trc_word_t* translated;
	trc_word_t offset;
	trc_convert_t convert;
	translated = WORD_BASE(addr);
	offset = SUBWORD(translated, addr, 1);
	convert.word[0] = trc_read(translated);
	DEBUG3("write_i8 read: %p %lx", translated, convert.word[0]);
	convert.i8[offset] = value;
	DEBUG3("write_i8 write: %p %lx", translated, convert.word[0]);
	trc_write(translated, convert.word[0]);
}

void trc_write_double(volatile double* addr, double value) {
	if (sizeof(trc_word_t) == 8) {
		trc_convert_t convert;
		convert.d[0] = value;
		trc_write((trc_word_t*)addr, convert.word[0]);
	} else {
		trc_convert_t convert;
		trc_word_t* translated;
		translated = (trc_word_t*)addr;
		convert.d[0] = value;
		trc_write(translated, convert.word[0]);
		trc_write(translated + 1, convert.word[1]);
	}

}

void trc_write_float(volatile float* addr, float value) {
	if (sizeof(trc_word_t) == 4) {
		trc_convert_t convert;
		convert.f[0] = value;
		trc_write((trc_word_t*)addr, convert.word[0]);
	} else {
		trc_word_t* translated;
		trc_word_t  offset;
		trc_convert_t convert;
		translated = WORD_BASE(addr);
		offset = SUBWORD(translated, addr, 4);
		convert.word[0] = trc_read(translated);
		DEBUG3("write_float read: %p %lx", translated, convert.word[0]);
		convert.f[offset] = value;
		DEBUG3("write_float write: %p %lx",translated, convert.word[0]);
		trc_write(translated, convert.word[0]);
	}
}

byte_t trc_size(void) {
	return 1;
}

byte_t trc_rank(void) {
	return 0;
}

void trc_barrier(void) {

}

void trc_share_pointer(void** ptr, void* content) {

}

byte_t trc_start_thread
(thread_t* ptr, byte_t node, void*(*func)(void*), void* args) {
	int ret;
	ptr->node = 0;
	ptr->thread_ptr = malloc(sizeof(pthread_t));
	ret = pthread_create(ptr->thread_ptr, 0, func, args);
	if (ret) return 0; else return 1;
}

byte_t trc_join_thread
(thread_t* ptr) {
	void* dummy;
	int ret;
	if (!ptr->thread_ptr) return 0;
	ret = pthread_join(*ptr->thread_ptr, &dummy);
	if (ret) return 0;
	free(ptr->thread_ptr);
	return 1;
}
