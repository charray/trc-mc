# TrC-MC: Transactional Consistency for Multicore Systems

To achieve single-lock atomicity in software transactional memory systems, the commit procedure often goes through a common clock variable.  When there are frequent transactional commits, clock sharing becomes inefficient.  Tremendous cache contention takes place between the processors and the computing throughput  no  longer scales with processor count.  Therefore, traditional transactional memories are unable to accelerate applications with frequent commits regardless of thread count.  While systems with decentralized data structures have better performance on these applications, we argue they are incomplete as they create much more aborts than traditional transactional systems.  We apply two design changes, namely zone partitioning and timestamp extension, to optimise an existing decentralized algorithm.

## Usage

With the `trcmc.h` header file, program your C program with the application interface provided.  Compile it together with the `trcmc.c` given.

To initialise, use the function `trc_init()` in the main program and `trc_thread_init()` for each thread that runs transaction.  To tear down, use the function `trc_thread_destroy()` before terminating each thread and function `trc_destroy()` to clean up the system resources.

Transactions can be made by making use of the `atomic {...}` macro blocks.  Therein, `trc_read_...()` and `trc_write_...()` functions can be used to access the shared memory space with consistency guarantee.

## Configuration

Inside the file `config.h`, one can define the preferred behaviour of the transactional memory system.  Some particular features are as follows:

 * `ZONE_ENABLE`: Enable the zoning technique that splits the transactional metadata into different zones.
 * `ZONE_BITS`: Number of bits from the timestamp to reserve for the zones.
 * `PAD_ZONES`: Number of zones plus this, together multiplied by the native word size, must be larger than the processor cache line size.
 * `TEXT_ENABLE`: Timestamp extension technique, as seen on the TinySTM paper and applied in TrC-MC.
 * `HINT_ENABLE`: Enable each transactional thread to monitor its consecutive commits or aborts.  Practically, the system does not perform any decision base on these information.
 * `REPORT_ENABLE`: Enable global variables to record.  This will slow things down by a lot, use another option below.
 * `REPORT2_ENABLE`: Enable thread-local variables to record system performance.  At the end of program execution, the aggreated results will be printed out to the standard output.
 * `PT_ENABLE`: Enable the "probing-throttle" adaptive strategy.  Some threads will be adaptively stalled to trade for better commit rate.
 * `MS_ENABLE`: Half-baked "mode-switching" adaptive strategy.  With this enabled, read and write procedures can change between agressive (AA), eager (EA), and lazy(LA) when a transaction starts.  Practically, the logic for picking a mode is missing.

## Application Interface

Transaction Management:

 * `void        trc_init(int*, char***)`
 * `void        trc_destroy()`
 * `void        trc_thread_init()`
 * `void        trc_thread_destroy()`
 * `void        trc_begin(trc_roll_t*, trc_hint_t*)`
 * `void        trc_abort()`
 * `void        trc_commit()`

Transaction Read / Write with Native Word-size Variables:

 * `trc_word_t  trc_read(volatile trc_word_t*)`
 * `void        trc_write(volatile trc_word_t*, trc_word_t)`
 * `trc_word_t* trc_malloc(trc_word_t)`
 * `trc_word_t* trc_realloc(trc_word_t*, trc_word_t)`
 * `void        trc_free(trc_word_t*)`
 * `void        trc_free2(trc_word_t*, trc_word_t)`

Transaction Read with Various Variable Lengths:

 * `void*       trc_read_ptr(volatile void**)`
 * `uint64_t    trc_read_u64(volatile uint64_t*)`
 * `uint32_t    trc_read_u32(volatile uint32_t*)`
 * `uint16_t    trc_read_u16(volatile uint16_t*)`
 * `uint8_t     trc_read_u8(volatile uint8_t*)`
 * `int64_t     trc_read_i64(volatile int64_t*)`
 * `int32_t     trc_read_i32(volatile int32_t*)`
 * `int16_t     trc_read_i16(volatile int16_t*)`
 * `int8_t      trc_read_i8(volatile int8_t*)`
 * `double      trc_read_double(volatile double*)`
 * `float       trc_read_float(volatile float*)`

Transaction Write with Various Variable Lengths:

 * `void        trc_write_ptr(volatile void**, void*)`
 * `void        trc_write_u64(volatile uint64_t*, uint64_t)`
 * `void        trc_write_u32(volatile uint32_t*, uint32_t)`
 * `void        trc_write_u16(volatile uint16_t*, uint16_t)`
 * `void        trc_write_u8(volatile uint8_t*, uint8_t)`
 * `void        trc_write_i64(volatile int64_t*, int64_t)`
 * `void        trc_write_i32(volatile int32_t*, int32_t)`
 * `void        trc_write_i16(volatile int16_t*, int16_t)`
 * `void        trc_write_i8(volatile int8_t*, int8_t)`
 * `void        trc_write_double(volatile double*, double)`
 * `void        trc_write_float(volatile float*, float)`

Cluster-Wide (for TrC-DC) Programming Interfaces, Mostly Dummy:

 * `byte_t      trc_size(void)`
 * `byte_t      trc_rank(void)`
 * `void        trc_barrier(void)`
 * `void        trc_share_pointer(void**, void*)`
 * `byte_t      trc_start_thread(thread_t*, byte_t, void*(*func)(void*), void*)`
 * `byte_t      trc_join_thread(thread_t*)`

## Supported Platforms

Currently, the program is only compilable in Linux with GNU compilers.  This is because the program has been written for the Gideon II cluster in the University of Hong Kong.  The program was tested on Intel Nehelam processors and also incidentally the IBM POWER 8 processors (with some undocumented changes).

In future, support for FreeBSD and Clang compilers will be possible.  The major porting difficulties lie with the assembly code syntax and the processor affinity system calls.

## Papers for Citation

 * Kinson Chan, King Tin Lam, and Cho-Li Wang, Cache Affinity Optimization Techniques for Scaling Software Transactional Memory Systems on Multi-CMP Architectures, The 14th International Symposium on Parallel and Distributed Computing (ISPDC 2015), Limassol, Cyprus, 29 June - 1 July 2015.
 * Kinson Chan and Cho-Li Wang, Optimizing Decentralized Software Transactional Memory for Multi-Multicore Computers, The 17th IEEE International Conference on Parallel and Distributed Systems (ICPADS 2011), Tainan, Taiwan, 7-9 December 2011. 
 * Kinson Chan, King Tin Lam, Cho-Li Wang, Adaptive Thread Scheduling Techniques for Improving Scalability of Software Transactional Memory, International Conference on Parallel and Distributed Computing and Networks (PDCN2011), Innsbruck, Austria, 15-17 Feburary 2011.
