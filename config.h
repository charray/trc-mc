/*
Copyright (c) 2009-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef _TRCMC_CONFIG_H_
#define _TRCMC_CONFIG_H_

#define ZONE_BITS      2
#define NUM_ZONES      (1<<ZONE_BITS)
#define PAD_ZONES      62
#define PROCESSORS     ""
#define ZONES          ""

#define NUM_TIMESTAMPS (1<<20)

// #define DEBUG0_ENABLE
// #define DEBUG_ENABLE
// #define DEBUG2_ENABLE
// #define DEBUG3_ENABLE
// #define DEBUG4_ENABLE

#define ZONE_ENABLE
#define TEXT_ENABLE

// #define HINT_ENABLE
// #define REPORT_ENABLE
#define REPORT2_ENABLE

// #define PT_ENABLE
#define PT_DENOM       32
#define PT_UPPER       31
#define PT_LOWER       4
#define PT_PERIOD      5000

#define MS_ENABLE
#define MS_DENOM       16
#define MS_LA_ENABLE   1
#define MS_EA_ENABLE   6
#define DEFAULT_MODE   EA

#define OPS_COUNT      (4096 / sizeof(trc_ops_t))

#endif
