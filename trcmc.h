/*
Copyright (c) 2009-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _TRCMC_H_
#define _TRCMC_H_

#include <setjmp.h>
#include <stdint.h>
#include <pthread.h>

typedef unsigned long  trc_word_t;
typedef unsigned short trc_quarter_t;
typedef unsigned char  trc_byte_t;
typedef sigjmp_buf     trc_roll_t;
#define word_t trc_word_t
#define quarter_t trc_quarter_t
#define byte_t trc_byte_t
#define roll_t trc_roll_t
#define thread_t trc_thread_t

typedef struct {
	char*      tag;
	trc_word_t commits;
	trc_word_t aborts;
} trc_hint_t;

typedef struct {
	byte_t node;
	pthread_t* thread_ptr;
} trc_thread_t;

void        trc_init(int*, char***);
void        trc_destroy();
void        trc_thread_init();
void        trc_thread_destroy();
void        trc_begin(trc_roll_t*, trc_hint_t*);
void        trc_abort();
void        trc_commit();
#define trc_exit trc_destroy
#define trc_thread_exit trc_thread_destroy

trc_word_t  trc_read(volatile trc_word_t*);
void        trc_write(volatile trc_word_t*, trc_word_t);
trc_word_t* trc_malloc(trc_word_t);
trc_word_t* trc_realloc(trc_word_t*, trc_word_t);
void        trc_free(trc_word_t*);
void        trc_free2(trc_word_t*, trc_word_t);

void*       trc_read_ptr(volatile void**);
uint64_t    trc_read_u64(volatile uint64_t*);
uint32_t    trc_read_u32(volatile uint32_t*);
uint16_t    trc_read_u16(volatile uint16_t*);
uint8_t     trc_read_u8(volatile uint8_t*);
int64_t     trc_read_i64(volatile int64_t*);
int32_t     trc_read_i32(volatile int32_t*);
int16_t     trc_read_i16(volatile int16_t*);
int8_t      trc_read_i8(volatile int8_t*);
double      trc_read_double(volatile double*);
float       trc_read_float(volatile float*);

void        trc_write_ptr(volatile void**, void*);
void        trc_write_u64(volatile uint64_t*, uint64_t);
void        trc_write_u32(volatile uint32_t*, uint32_t);
void        trc_write_u16(volatile uint16_t*, uint16_t);
void        trc_write_u8(volatile uint8_t*, uint8_t);
void        trc_write_i64(volatile int64_t*, int64_t);
void        trc_write_i32(volatile int32_t*, int32_t);
void        trc_write_i16(volatile int16_t*, int16_t);
void        trc_write_i8(volatile int8_t*, int8_t);
void        trc_write_double(volatile double*, double);
void        trc_write_float(volatile float*, float);

byte_t      trc_size(void);
byte_t      trc_rank(void);
void        trc_barrier(void);
void        trc_share_pointer(void**, void*);
byte_t      trc_start_thread(thread_t*, byte_t, void*(*func)(void*), void*);
byte_t      trc_join_thread(thread_t*);

#define TRC_STRINGIFY(x) #x
#define TRC_TOSTRING(x) TRC_STRINGIFY(x)
#define TRC_AT __FILE__ ":" TRC_TOSTRING(__LINE__)
#define TRC_CONCATENATE_DETAIL(x, y) x##y
#define TRC_CONCATENATE(x, y) TRC_CONCATENATE_DETAIL(x, y)

#define atomic \
trc_roll_t TRC_CONCATENATE(_trc_roll_, __LINE__); \
static trc_hint_t TRC_CONCATENATE(_trc_hint_, __LINE__) = {TRC_AT, 0, 0}; \
trc_word_t TRC_CONCATENATE(_trc_retry_, __LINE__); \
sigsetjmp(TRC_CONCATENATE(_trc_roll_, __LINE__), 0); \
for(TRC_CONCATENATE(_trc_retry_, __LINE__) = 1, trc_begin(&TRC_CONCATENATE(_trc_roll_, __LINE__), &TRC_CONCATENATE(_trc_hint_, __LINE__)); TRC_CONCATENATE(_trc_retry_, __LINE__) == 1; (trc_commit()), TRC_CONCATENATE(_trc_retry_, __LINE__) = 0)

#endif

