#!/usr/bin/perl
use strict;
use warnings;
use List::Util 'max';

my $info_str = `cat /proc/cpuinfo`;
my @info_array = split(/\n/, $info_str);

my @procs = grep(/^processor\s+:/, @info_array);
my @phys = grep(/^physical id\s+:/, @info_array);
my @cores = grep(/^cpu cores\s+:/, @info_array);

foreach (@procs) { $_ =~ s/[^0-9]//g; }
foreach (@phys) { $_ =~ s/[^0-9]//g; }
foreach (@cores) { $_ =~ s/[^0-9]//g; }

my $max_proc_id = max(@procs);
my $max_phy_id = max(@phys);
my @max_cores = ();
$max_cores[$phys[$_]] = $cores[$_] for (0..$#cores);

my @sorted = ();
for my $i (0..$max_phy_id) {
	my @members = ();
	for (0..$#phys) { push @members, $procs[$_] if ($phys[$_] == $i); }
	push @sorted, [ @members ];
}

my $more = 1;
my $answer1 = "";
my $answer2 = "";
while ($more == 1) {
	$more = 0;
	for my $i (0..$max_phy_id) {
		for (1..$max_cores[$i]) {
			my $core = shift @{ $sorted[$i] };
			last unless defined $core;
			$answer1 = "$answer1,$core";
			$answer2 = "$answer2,$i";
			$more = 1;
		}
	}
}
$answer1 = substr $answer1, 1;
$answer2 = substr $answer2, 1;
print "PROCESSOR_IDS=$answer1\n";
print "PROCESSOR_ZONES=$answer2\n";

