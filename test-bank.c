/*
Copyright (c) 2009-2013,2017, Kinson Chan
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "trcmc.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

long* accounts;
int my_rank;
int our_size;

long random_acc() {
	long result;
	result = labs((long)rand() * (long)rand()) % (our_size * 1000000);
	return result;
}

void* work_thread(void* args) {
	int i;
	long a;
	long b;
	for (i = 0; i < 100000; ++i) {
		a = random_acc();
		b = random_acc();
		atomic {
			trc_write_i64(&accounts[a],
				trc_read_i64(&accounts[a]) + 1);
			
			trc_write_i64(&accounts[b],
				trc_read_i64(&accounts[b]) - 1);
		}
		// if (!(i % 10000)) printf("%ld\n", i);
	}
	return 0;
}

void init(int count) {
	accounts = (long*)trc_malloc(count * 1000000 * sizeof(long));
	trc_share_pointer((void*)&accounts, accounts);
}

void verify(int count) {
	long i;
	long sum = 0;
	for (i = 0; i < count * 1000000; ++i) {
		sum += accounts[i];
	}
	printf("Final Result: %ld\n", sum);
}

void work(int count) {
	int i;
	thread_t* threads;
	threads = (thread_t*)malloc(sizeof(thread_t) * count);
	for (i = 0; i < count; ++i) {
		trc_start_thread(&threads[i], i * trc_size() / count,
			work_thread, (void*)(long)i);
	}

	for (i = 0; i < count; ++i) {
		trc_join_thread(&threads[i]);
		// printf("joined %d: %d\n", i, trc_join_thread(&threads[i]));
	}
}

int main(int argc, char** argv) {
	int count;
	struct timeval start, end;

	trc_init(&argc, &argv);
	my_rank = trc_rank();
	our_size = trc_size();
	if (argc != 2) {
		printf("Usage: %s <num_thread>\n", argv[0]);
		exit(0);
	}
	count = our_size * atoi(argv[1]);
	trc_barrier();
	if (!my_rank) init(count);
	gettimeofday(&start, 0);
	if (!my_rank) work(count);
	gettimeofday(&end, 0);
	trc_barrier();
	if (!my_rank) {
		printf("Time taken: %f seconds\n",
			end.tv_sec - start.tv_sec +
			(end.tv_usec - start.tv_usec) / 1000000.0);
	}
	verify(count);
	trc_exit();
	return 0;
}

